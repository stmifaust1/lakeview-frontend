$(document).ready(function() {
    $(window).bind('scroll', function(e) {
        parallax();
    });
});

function parallax() {
    var scrollPosition = $(window).scrollTop();
    $('.wave-1').css('top',(0 - (scrollPosition * -.2))+'px' );
    $('.wave-2').css('top',(-40 - (scrollPosition * .2))+'px' );
}       